
// Colby Witthuhn
// Calculator Assignment 2

#include <iostream>
#include <conio.h>

using namespace std;

float Add(float num1, float num2)
{
	float sum = num1 + num2;
	return sum;
}

float Subtract(float num1, float num2)
{
	float difference = num1 - num2;
	return difference;
}

float Multiply(float num1, float num2)
{
	float product = num1 * num2;
	return product;
}

bool Divide(float num1, float num2, float &answer)
{
	if (num1 != 0 && num2 != 0)
	{
		answer = num1 / num2;
		return true;
	}
	return false;
}
int main()
{
	char again = 'y';
	char sign = '+';
	float num1 = 0;
	float num2 = 0;
	while (again == 'y')
	{
		cout << "Please enter two positive numbers, and one of the follow operators: +, -, *, / \n";
		cin >> num1; cin >> num2; cin >> sign;
		if (sign == '+')
		{
			cout << num1 << " + " << num2 << " = " << Add(num1, num2) << "\n";
		}
		else if (sign == '-')
		{
			cout << num1 << " - " << num2 << " = " << Subtract(num1,num2) << "\n";

		}
		else if (sign == '*')
		{
			cout << num1 << " * " << num2 << " = " << Multiply(num1,num2) << "\n";
		}
		else if (sign == '/')
		{
			float answer;
			if (Divide(num1, num2, answer))
			{
				cout << num1 << " / " << num2 << " = " << answer << "\n";
			}
			else
			{
				cout << "Please don't use zeros with division\n";
			}
		}
		else
		{
			"Please enter a valid operator";
		}
		cout << "Would you like to go again? Type 'y' if yes, or 'n' if no\n";
		cin >> again;
	}
	(void)_getch();
	return 0;
}

